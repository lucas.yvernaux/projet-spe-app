<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MangaController;
use App\Http\Controllers\FaceBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome-app');
});

/*Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');*/

Route::middleware('auth')->group(function () {
    Route::get('/home', [MangaController::class, 'index'])->name('home');
    Route::get('/search', [MangaController::class, 'search'])->name('search');
    Route::get('/searchAdvance', [MangaController::class, 'searchAdvance'])->name('searchAdvance');
    Route::get('/manga/{idManga}', [MangaController::class, 'show'])->whereNumber('idManga')->name('manga.info');
    Route::get('/manga/add', [MangaController::class, 'create'])->name('add');
    Route::post('/manga/create', [MangaController::class, 'store'])->name('manga.create');
    Route::get('/manga/edit', [MangaController::class, 'edit'])->name('manga.edit');
    Route::get('/manga', [MangaController::class, 'showMyManga'])->name('manga.list');
    Route::patch('/manga', [MangaController::class, 'update'])->name('manga.update');
    Route::post('/manga/destroy', [MangaController::class, 'destroy'])->name('manga.destroy');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::fallback(function() {
    return view('404'); // la vue 404.blade.php
});

// Facebook Login URL
Route::prefix('facebook')->name('facebook.')->group( function(){
    Route::get('auth', [FaceBookController::class, 'loginUsingFacebook'])->name('login');
    Route::get('callback', [FaceBookController::class, 'callbackFromFacebook'])->name('callback');
});
require __DIR__.'/auth.php';
