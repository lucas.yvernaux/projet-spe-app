<x-guest-layout>
    <div class="full-screen ctn-center">
        <div class="ctn-scale text-center p-4">
            <h1 class="big-title">HAWK ,<br> your nakama of Japan</h1>
            <div style="height: 50px"></div>
            <div class="flex flex-col gap-4">
                <a href="{{ route('register') }}">
                    <button style="width: 100%;
                            background-color: var(--main-color);
                            color: #FFF;
                            border-radius: 50vmax;
                            padding: 16px 24px;">Register</button>
                </a>
                <a href="{{ route('login') }}">
                    <button  style="width: 100%;
                            background-color: var(--main-color);
                            color: #FFF;
                            border-radius: 50vmax;
                            padding: 16px 24px;">Log In</button>
                </a>
                <a href="{{ route('facebook.login') }}">
                    <img style="width:24px;" src="{{ asset('images/logo/facebook.png') }}"/>
                </a>
            </div>
        </div>
    </div>
</x-guest-layout>
