@if ($message = Session::get('success'))
<x-modal name="confirm-purchase" :show="true" focusable>
    <div class="ctn-modal">
        <a href="">
            <img class="mr-2 mt-2 btn-close" style="float: right;" src="{{ asset('images/picto/close-color.png') }}" alt="CTA close"/>
        </a>
        <div class="flex flex-col items-center p-6 gap-8">
            <h2 class="font-bold text-center text-white" style="font-size: 24px">{{ $message }}</h2>
        </div>
    </div>
</x-modal>
@endif


@if ($message = Session::get('error'))
<x-modal name="confirm-purchase" :show="true" focusable>
    <div class="ctn-modal">
        <a href="">
            <img class="mr-2 mt-2 btn-close" style="float: right;" src="{{ asset('images/picto/close-color.png') }}" alt="CTA close"/>
        </a>
        <div class="flex flex-col items-center p-6 gap-8">
            <h2 class="font-bold text-center text-white" style="font-size: 24px">Une erreur est survenue</h2>
            <h2 class="font-bold text-center text-white" style="font-size: 24px">{{ $message }}</h2>
        </div>
    </div>
</x-modal>
@endif


@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
    <a href="">
        <img class="mr-2 mt-2 btn-close" style="float: right;" src="{{ asset('images/picto/close-color.png') }}" alt="CTA close"/>
    </a>    <strong>{{ $message }}</strong>
</div>
@endif


@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
    <a href="">
        <img class="mr-2 mt-2 btn-close" style="float: right;" src="{{ asset('images/picto/close-color.png') }}" alt="CTA close"/>
    </a>    <strong>{{ $message }}</strong>
</div>
@endif


@if ($errors->any())
<div class="alert alert-danger">
    <a href="">
        <img class="mr-2 mt-2 btn-close" style="float: right;" src="{{ asset('images/picto/close-color.png') }}" alt="CTA close"/>
    </a>
    Please check the form below for errors
</div>
@endif
