@props(['requeteValue' => ''])
<style>
    span#searchBar input[name="search"] {
        flex: 8;
    }
    span#btnFilter {
        background: url("{{ asset('images/picto/settings-color.png') }}") no-repeat center;
        content: '';
        width: 100%;
        flex: 1;
    }
    section#section-content-filter {
        gap: 8px;
        justify-content: center;
        margin-top: 8px;
    }
</style>

<form action="{{ route('search') }}" method="GET">
    <span id="searchBar" class="flex">
        <input
            class="mt-3 placeholder-italic placeholder-slate-400 block bg-white w-full border border-slate-300 rounded-full py-2 pr-9 pl-3 sm:text-sm"
            placeholder="Search ..." value="{{ $requeteValue }}" type="search" name="search" />
        <span id="btnFilter"></span>
    </span>
    <section id="section-container-filter" class="hidden">
        <section id="section-content-filter" class="flex">
            <select name="category" id="category" class="full-width"
                    style="border: 1px solid var(--divider-color); border-radius: 50vmax; font-size: 14px;"
                    required >
                <option value="unknown">Select a category</option>
                <option value="manga">manga</option>
                <option value="novel">novel</option>
                <option value="lightnovel">light novel</option>
                <option value="oneshot">one shot</option>
                <option value="doujin">doujin</option>
                <option value="manhwa">manhwa</option>
                <option value="manhua">manhua</option>
            </select>
            <select name="status" id="status" class="full-width"
                    style="border: 1px solid var(--divider-color); border-radius: 50vmax; font-size: 14px;"
                    required >
                <option value="unknown">Select a status</option>
                <option value="publishing">publishing</option>
                <option value="complete">complete</option>
                <option value="hiatus">hiatus</option>
                <option value="discontinued">discontinued</option>
                <option value="upcoming">upcoming</option>
            </select>
        </section>
        <a class="text-center block bg-white border border-slate-300 rounded-full py-2 px-3 "
           href="{{ route('searchAdvance') }}"
           style="
            width: fit-content;
            margin: 12px auto;
            ">Recherche Avancé</a>
    </section>
    <input type="hidden" name="numPage" value="1" />
</form>


<script type="text/javascript">
    const toggleFilter = () => {
        const sectionFilter = document.querySelector('#section-container-filter');
        if(sectionFilter.className.includes('hidden'))
            sectionFilter.className = 'flex-col';
        else
            sectionFilter.className = 'hidden';
    }
    document.querySelector('span#btnFilter').addEventListener("click", toggleFilter);
</script>
