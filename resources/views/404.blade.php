<x-guest-layout>
    <div class="full-screen ctn-center">
        <div class="ctn-scale text-center p-4">
            <h1 class="big-title">Error 404</h1>
            <div style="height: 50px"></div>
            <div class="flex flex-col gap-4">
                <p class="text-white">Page not found</p>
            </div>
        </div>
    </div>
</x-guest-layout>
