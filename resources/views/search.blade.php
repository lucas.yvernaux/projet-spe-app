<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Result Search Manga') }}
        </h2>
    </x-slot>
    <x-slot name="header">
        <a style="color: white;" href="{{  url()->previous() }}">Back</a>
    </x-slot>

    <x-searchBar :requeteValue="$requeteSearch['search']"/>

    <div class="ctn-scaled flex mt-8">
        <div class="p-6" style="display: flex; flex-direction: column; gap: 20px; align-items: center">
            @isset($resultSearch)
                @foreach ($resultSearch as $allMangas)
                    @foreach ($allMangas as $mangas)
                        <section id="section-pagination-top" class="flex bg-gray-100">
                            <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                @php
                                $isFirstPage = $mangas->pagination->current_page == 1 ? 'true' : 'false';
                                $isDisabled = $isFirstPage == 'true' ? 'disabled' : '';
                                @endphp
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page - 1 }}" />

                                <button class="mr-2 {{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/left-chevron.png') }}"></button>
                            </form>
                            @for ($i=1 ; $i <= $mangas->pagination->last_visible_page;$i++)
                            <form action="{{ route('search') }}" method="GET">
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $i }}" />
                                @php
                                $color = $i == $mangas->pagination->current_page ? 'var(--main-color)' : 'black';
                                @endphp
                                <button style="color: {{ $color }}" class="mr-2" type="submit">{{ $i }}</button>
                            </form>
                            @endfor
                            <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                @php
                                $isDisabled = !($mangas->pagination->has_next_page) ? 'disabled' : '';
                                @endphp
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page + 1 }}" />

                                <button class="{{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/chevron.png') }}"></button>
                            </form>
                        </section>
                        @foreach ($mangas->data as $manga)
                            <a class="flex items-center flex-col" href='/manga/{{ $manga->mal_id }}'>
                                <img width="225" height="320" src="{{ $manga->images->webp->image_url }}" alt="image couverture manga">
                                <h2 class="text-center" style="color: var(--main-color)">{{ $manga->title }}</h2>
                            </a>
                        @endforeach
                    @endforeach
                    <section id="section-pagination-bot" class="flex bg-gray-100">
                        <form class="flex items-center" action="{{ route('search') }}" method="GET">
                            @php
                                $isFirstPage = $mangas->pagination->current_page == 1 ? 'true' : 'false';
                                $isDisabled = $isFirstPage == 'true' ? 'disabled' : '';
                            @endphp
                            <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                            <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                            <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                            <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page - 1 }}" />

                            <button class="mr-2 {{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/left-chevron.png') }}"></button>
                        </form>
                        @for ($i=1 ; $i <= $mangas->pagination->last_visible_page;$i++)
                            <form action="{{ route('search') }}" method="GET">
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $i }}" />
                                @php
                                    $color = $i == $mangas->pagination->current_page ? 'var(--main-color)' : 'black';
                                @endphp
                                <button style="color: {{ $color }}" class="mr-2" type="submit">{{ $i }}</button>
                            </form>
                        @endfor
                        <form class="flex items-center" action="{{ route('search') }}" method="GET">
                            @php
                                $isDisabled = !($mangas->pagination->has_next_page) ? 'disabled' : '';
                            @endphp
                            <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                            <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                            <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                            <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page + 1 }}" />

                            <button class="{{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/chevron.png') }}"></button>
                        </form>
                    </section>
                @endforeach

            @endisset
        </div>
    </div>
</x-app-layout>
