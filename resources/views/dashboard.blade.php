<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="full-screen ctn-center">
        <div class="ctn-scale text-center p-4">
            <h2 class="text-xl font-bold">Welcome to MANGA COLLEC</h2>
            <p>For beginning add manga to list them, give them away or exchange them !</p>
            <img class="img-position" src="{{ asset('images/img-legumes&fruits.png') }}" alt="">
            <div class="flex flex-col gap-4">
                <a href="{{ route('add') }}">
                    <x-primary-button>Add Manga</x-primary-button>
                </a>
                <a class="underline" href="{{ route('home') }}">
                    {{ __('Later') }}
                </a>
            </div>
        </div>
    </div>
</x-app-layout>
