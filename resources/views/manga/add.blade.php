<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Add Manga') }}
        </h2>
    </x-slot>

    <div class="ctn-scaled">
        <form method="POST" action={{ route('manga.create') }}>
        @csrf
        @method('post')

        <!-- Photo produit -->
        <!-- Feature Image a venir
            <div class="mt-4">
                <x-input-label for="file" :value="__('Ajouter une photo')" />
                <div style="position: relative;">
                    <input id="file"
                                type="file"
                                name="file"
                                accept=".png, .jpg, .jpeg"
                                style="z-index: 3; position: relative; opacity: 0; width: 80px; height: 80px;"
                                required
                                autofocus
                                onchange="showPreview(event);" />
                    <div class="input-file" style="display: flex; position: absolute; inset: 0 0 0 0; z-index: 2">
                        <div id="fake-input-file" style="display: grid; place-content: center; width: 80px; height: 80px; object-fit: cover; border: 1px solid var(--divider-color); border-radius: 16px; color: var(--divider-color); font-size: 24px">+</div>
                        <div id="ctn-preview-img" style="position: absolute; display: none">
                            <img src="{{ asset('images/close-circle.svg') }}" alt="retirer" style="cursor: pointer; z-index: 10; position: absolute; inset: -5px -5px auto auto; width: 20px; height: 20px; background: #fff; border-radius: 50%" onclick="closePreview();">
                            <img id="preview-img" style="width: 80px; height: 80px; object-fit: cover; border-radius: 16px;" src="">
                        </div>
                    </div>
                </div>
            </div>
        -->
        <!-- Nom du produit -->
        <div  class="mt-4">
            <x-input-label for="name" :value="__('Title Manga')" />
            <x-text-input id="name" class="full-width"
                          type="text"
                          name="name"
                          placeholder="One Piece"
                          required
                          autofocus />
            <!-- <x-input-error :messages="$errors->get('product_name')" class="mt-2" /> -->
        </div>

        <!-- Description du produit -->
        <div  class="mt-4">
            <x-input-label for="description" :value="__('Description')" />
            <textarea id="description" class="full-width"
                      style="border: 1px solid var(--divider-color); border-radius: 8px; font-size: 14px; resize: none;"
                      name="description"
                      placeholder="Description"
                      rows="4"
                      minlength="0"
                      maxlength="150"
                      required
                      autofocus ></textarea>

            <!-- <x-input-error :messages="$errors->get('product_name')" class="mt-2" /> -->
        </div>

        <!-- Catégorie -->
        <div  class="mt-4">
            <x-input-label for="category" :value="__('Category')" />
            <select name="category" id="category" class="full-width"
                    style="border: 1px solid var(--divider-color); border-radius: 50vmax; font-size: 14px;"
                    required >
                <option value="shonen">Select a category</option>
                <option value="shonen">shonen</option>
                <option value="seinen">seinen</option>
                <option value="shojo">shojo</option>
                <option value="isekai">isekai</option>
                <option value="hentai">hentai</option>
            </select>
            <!-- <x-input-error :messages="$errors->get('product_name')" class="mt-2" /> -->
        </div>

        <div  class="mt-4">
            <x-primary-button>{{ __('Add Manga') }}</x-primary-button>
            <div  class="mt-4">

                </form>
            </div>
</x-app-layout>

<script>
    const ctnPreview = document.querySelector("#ctn-preview-img");
    const preview = document.querySelector("#preview-img");
    const fakeInput = document.querySelector("#fake-input-file");
    const inputImage = document.querySelector("#url_image");

    function showPreview(event) {
        if(event.target.files.length > 0) {
            document.querySelector("#file").style.visibility = "hidden";
            ctnPreview.style.display = "block";
            let tmp = URL.createObjectURL(event.target.files[0]);
            preview.src = tmp
            inputImage.value = preview.src;
            console.log("test2 : "+inputImage)
        }
    }

    function closePreview() {
        ctnPreview.style.display = "none";
        document.querySelector("#file").style.visibility = "visible";
        document.querySelector("#file").value = "";
    }
</script>
