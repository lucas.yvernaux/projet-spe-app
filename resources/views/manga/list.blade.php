<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('My Manga Followed') }}
        </h2>
    </x-slot>

    <form action="{{ route('search') }}" method="GET">
        <input
            class="mt-2 placeholder-italic placeholder-slate-400 block bg-white w-full border border-slate-300 rounded-full py-2 pr-9 pl-3 sm:text-sm"
            placeholder="Search ..." type="search" name="search" />
        <input type="hidden" name="numPage" value="1" />
    </form>

    <div class="ctn-scaled flex mt-8">
        <div style="display: flex; flex-direction: column; gap: 20px; padding-bottom: 20px; margin:0 auto">
            @isset($result)
                @foreach ($result as $manga)
                    <a class="flex" style="flex-direction: column" href='/manga/{{ $manga->data->mal_id }}'>
                        <img width="225" height="320" src="{{ $manga->data->images->webp->image_url }}" alt="image couverture manga">
                        <h2 class="text-center" style="color: var(--main-color)">{{ $manga->data->title }}</h2>
                    </a>
                @endforeach
            @endisset
        </div>
    </div>
</x-app-layout>
