<x-app-layout>
    <x-slot name="header">
        <a class="text-gray-200" href="{{ url()->previous() }}">Back</a>
    </x-slot>

    <div class="ctn-scaled flex mt-8">
        <div style="display: flex; flex-direction: column; gap: 20px; margin:0 auto; align-items: center;">
            @isset($manga)
                <section id="section-follow" class="flex justify-end w-full">
                    @if($followed)
                        <form method="post" action="{{ route('manga.destroy') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ Auth::id() }}"/>
                            <input type="hidden" name="manga_id" value="{{ $manga->data->mal_id }}"/>
                            <button class="mr-8" type="submit">
                                <img src="{{ asset('images/picto/heart.png') }}" alt="picto heart for follow">
                            </button>
                        </form>
                    @else
                        <form method="post" action="{{ route('manga.create') }}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ Auth::id() }}"/>
                            <input type="hidden" name="manga_id" value="{{ $manga->data->mal_id }}"/>
                            <button class="mr-8" type="submit">
                                <img src="{{ asset('images/picto/heart-red.png') }}" alt="picto heart for follow">
                            </button>
                        </form>
                    @endif
                </section>
                <h2 class="text-xl font-bold tracking-widest" style="color: var(--main-color)" >{{ $manga->data->title }}</h2>
                <a href="{{ $manga->data->url }}" target="_blank">
                    <img width="225" height="320" src="{{ $manga->data->images->webp->image_url }}" alt="image couverture de {{ $manga->data->title }}">
                </a>
                <section id="section-content" class="bg-gray-100 border border-red-800 rounded-lg p-6" style="width: 80%;">
                    <section id="section-rank" style="margin: 0 auto; width: fit-content;">
                        @php
                        $nbStar = floor($manga->data->score/2);
                        @endphp
                        @if($nbStar == 0)
                        <img style="display: inline" src="{{ asset('images/picto/star-empty.png') }}" alt="icon star">
                        @else
                        @for ($i=0; $i < $nbStar; $i++)
                        <img style="display: inline" src="{{ asset('images/picto/star.png') }}" alt="icon star">
                        @endfor
                        @endif
                        <span class="relative" style="top: 2px;">{{ $manga->data->score }}</span>
                    </section>
                    <section id="section-author">
                        <h2 class="font-bold text-lg">Author</h2>
                        @forelse ($manga->data->authors as $author)
                        <span>{{ $author->name }}</span>
                        @empty
                        <span>Unknown</span>
                        @endforelse
                    </section>
                    <section id="section-genre">
                        <h2 class="font-bold text-lg">Genre</h2>
                        <ul style="list-style: inside">
                            @forelse ($manga->data->genres as $genre)
                            <li>{{ $genre->name }}</li>
                            @empty
                            <li style="list-style: none">Unknown</li>
                            @endforelse
                        </ul>
                    </section>
                    <section id="section-theme">
                        <h2 class="font-bold text-lg">Theme</h2>
                        <ul style="list-style: inside">
                            @forelse ($manga->data->themes as $theme)
                            <li>{{ $theme->name }}</li>
                            @empty
                            <li style="list-style: none">Unknown</li>
                            @endforelse
                        </ul>
                    </section>
                    <section id="section-type">
                        <h2 class="font-bold text-lg">Type</h2>
                        <ul style="list-style: inside">
                            @forelse ($manga->data->demographics as $type)
                            <li>{{ $type->name }}</li>
                            @empty
                            <li style="list-style: none">Unknown</li>
                            @endforelse
                        </ul>
                    </section>
                    <section id="section-magazine">
                        <h2 class="font-bold text-lg">Magazine</h2>
                        <ul style="list-style: inside">
                            @forelse ($manga->data->serializations as $serialization)
                            <li>{{ $serialization->name }}</li>
                            @empty
                            <li style="list-style: none">Unknown</li>
                            @endforelse
                        </ul>
                    </section>
                    <section id="section-status">
                        <h2 class="font-bold text-lg">Status</h2>
                        <p>{{ $manga->data->status }}</p>
                        @php
                        if($manga->data->published->to == null)
                        $dateFin = date_format(now()->tz('Europe/Paris'),"d/m/Y");
                        else
                        $dateFin = date("d/m/Y",strtotime($manga->data->published->to))
                        @endphp
                        <span>published from {{ date("d/m/Y",strtotime($manga->data->published->from)) }} to {{ $dateFin }}</span>
                    </section>
                </section>
                <section id="section-magazines" class="bg-gray-100 border border-red-800 rounded-lg p-6" style="width: 80%;">
                    <section>
                        <h2 class="font-bold text-lg border-b-2 mb-4">Official Website</h2>
                        @forelse ($providers->data as $provider)
                            <p class="link">
                                <a href="{{ $provider->url }}" target="_blank">{{ $provider->name }}</a>
                            </p>
                        @empty
                            <p>No Website</p>
                        @endforelse
                    </section>
                </section>

            @endisset
        </div>
    </div>
</x-app-layout>
