<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Result Search Advanced Manga') }}
        </h2>
    </x-slot>

    <form action="{{ route('searchAdvance') }}" method="GET">
    <span id="searchBar" class="flex">
        <input
            class="mt-3 placeholder-italic placeholder-slate-400 block bg-white w-full border border-slate-300 rounded-full py-2 pr-9 pl-3 sm:text-sm"
            placeholder="Search ..." value="{{ isset($requeteValue) ? $requeteValue : '' }}" type="search" name="search" />
    </span>
        <section id="section-content-filter" class="flex flex-col gap-6">
            <div id="group-select" class="flex justify-center mt-8 gap-6">
                <select name="category" id="category" class="full-width" style="border: 1px solid var(--divider-color); border-radius: 50vmax; font-size: 14px;" required="">
                    <option value="unknown">Select a category</option>
                    <option value="manga">manga</option>
                    <option value="novel">novel</option>
                    <option value="lightnovel">light novel</option>
                    <option value="oneshot">one shot</option>
                    <option value="doujin">doujin</option>
                    <option value="manhwa">manhwa</option>
                    <option value="manhua">manhua</option>
                </select>
                <select name="status" id="status" class="full-width" style="border: 1px solid var(--divider-color); border-radius: 50vmax; font-size: 14px;" required="">
                    <option value="unknown">Select a status</option>
                    <option value="publishing">publishing</option>
                    <option value="complete">complete</option>
                    <option value="hiatus">hiatus</option>
                    <option value="discontinued">discontinued</option>
                    <option value="upcoming">upcoming</option>
                </select>
            </div>
            <p class="text-white text-center">Score</p>
            <div id="group-score" class="flex gap-4 justify-center">
                <label for="scoreMin" class="text-white flex items-center">Min</label>
                <input type="text" name="scoreMin" value="0" max="10" maxlength="1"minlength="1"  class="w-16">
                <input type="text" name="score" value="1"  maxlength="2" minlength="1" max="10" class="w-16">
                <input type="text" name="scoreMax" value="10"  maxlength="2" minlength="1" max="10" class="w-16">
                <label for="scoreMax" class="text-white flex items-center">Max</label>
            </div>
            <div id="group-limit" class="flex justify-center gap-4">
                <label for="limit" class="text-white flex items-center">Result per page :</label>
                <input type="number" name="limit" value="25" min="1" max="25" class="w-16">
                <span class="text-white flex items-center text-xs">(max 25)</span>
            </div>
            <div id="btn-submit" class="flex justify-center">
                <input type="submit" value="Search" class="flex justify-center block bg-white border border-slate-300 rounded-full py-2 px-3">
            </div>
        </section>
        <input type="hidden" name="numPage" value="1" />
    </form>

    <div class="ctn-scaled flex mt-8">
        <div class="p-6" style="display: flex; flex-direction: column; gap: 20px; align-items: center">
            @isset($resultSearch)
                @foreach ($resultSearch as $allMangas)
                    @foreach ($allMangas as $mangas)
                        <p class="text-white"> Total Result {{ $mangas->pagination->items->total }}</p>
                        @if($mangas->pagination->items->total == 0)
                            <div style="width: 100vw;">
                                <h2 class="medium-title">No result, sorry :/ !</h2>
                            </div>
                        @else
                        <section id="section-pagination-bot" class="flex bg-gray-100">
                            <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                @php
                                    $isFirstPage = $mangas->pagination->current_page == 1 ? 'true' : 'false';
                                    $isDisabled = $isFirstPage == 'true' ? 'disabled' : '';
                                @endphp
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page - 1 }}" />

                                <button class="mr-2 {{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/left-chevron.png') }}"></button>
                            </form>
                            @for ($i=1 ; $i <= $mangas->pagination->last_visible_page;$i++)
                                <form action="{{ route('search') }}" method="GET">
                                    <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                    <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                    <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                    <input type="hidden" name="numPage" value="{{ $i }}" />
                                    @php
                                        $color = $i == $mangas->pagination->current_page ? 'var(--main-color)' : 'black';
                                    @endphp
                                    <button style="color: {{ $color }}" class="mr-2" type="submit">{{ $i }}</button>
                                </form>
                            @endfor
                            <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                @php
                                    $isDisabled = !($mangas->pagination->has_next_page) ? 'disabled' : '';
                                @endphp
                                <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page + 1 }}" />

                                <button class="{{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/chevron.png') }}"></button>
                            </form>
                        </section>
                            @foreach ($mangas->data as $manga)
                                <a class="flex items-center flex-col" href='/manga/{{ $manga->mal_id }}'>
                                    <img width="225" height="320" src="{{ $manga->images->webp->image_url }}" alt="image couverture manga">
                                    <h2 class="text-center" style="color: var(--main-color)">{{ $manga->title }}</h2>
                                </a>
                            @endforeach
                            <section id="section-pagination-bot" class="flex bg-gray-100">
                                <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                    @php
                                        $isFirstPage = $mangas->pagination->current_page == 1 ? 'true' : 'false';
                                        $isDisabled = $isFirstPage == 'true' ? 'disabled' : '';
                                    @endphp
                                    <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                    <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                    <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                    <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page - 1 }}" />

                                    <button class="mr-2 {{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/left-chevron.png') }}"></button>
                                </form>
                                @for ($i=1 ; $i <= $mangas->pagination->last_visible_page;$i++)
                                    <form action="{{ route('search') }}" method="GET">
                                        <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                        <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                        <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                        <input type="hidden" name="numPage" value="{{ $i }}" />
                                        @php
                                            $color = $i == $mangas->pagination->current_page ? 'var(--main-color)' : 'black';
                                        @endphp
                                        <button style="color: {{ $color }}" class="mr-2" type="submit">{{ $i }}</button>
                                    </form>
                                @endfor
                                <form class="flex items-center" action="{{ route('search') }}" method="GET">
                                    @php
                                        $isDisabled = !($mangas->pagination->has_next_page) ? 'disabled' : '';
                                    @endphp
                                    <input type="hidden" name="search" value="{{ $requeteSearch['search'] }}"/>
                                    <input type="hidden" name="category" value="{{ $requeteSearch['type'] }}"/>
                                    <input type="hidden" name="status" value="{{ $requeteSearch['status'] }}"/>
                                    <input type="hidden" name="numPage" value="{{ $mangas->pagination->current_page + 1 }}" />

                                    <button class="{{ $isDisabled }}" type="submit"><img class="mr-2" src="{{ asset('images/picto/chevron.png') }}"></button>
                                </form>
                            </section>
                        @endif
                    @endforeach
                @endforeach
            @endisset
        </div>
    </div>
</x-app-layout>
