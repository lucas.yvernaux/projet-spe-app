<?php

namespace App\Http\Controllers;

use App\Models\Manga;
use App\Http\Requests\StoreMangaRequest;
use App\Http\Requests\UpdateMangaRequest;

use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;

class MangaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index() : View
    {
        $result[] = array();

        for ($i=1; $i <=4; $i++) {
            array_unshift($result,json_decode(Http::get('https://api.jikan.moe/v4/manga/'.$i)->getBody()->getContents()));
            sleep(1);
        }
        array_pop($result);
        return view('home',compact('result'));
    }

    /**
     * Display a listing of my mangas.
     */
    public function showMyManga() : View
    {
        $result[] = array();

        $mangas = Manga::where('user_id',Auth::id())->get();

        for ($i=0; $i <count($mangas); $i++) {
            array_unshift($result,json_decode(Http::get('https://api.jikan.moe/v4/manga/'.$mangas[$i]->manga_id)->getBody()->getContents()));
            sleep(1);
        }
        array_pop($result);
        return view('manga.list', compact('result'));
    }

    public function search(Request $request) : View
    {

        $resultSearch[] = array();
        $requeteSearch[] = array();

        // Get the search value from the request
        $search = $request->input('search');
        $numPage = $request->input('numPage');
        $mangaType = $request->input('category');
        $mangaStatus = $request->input('status');

        $requeteSearch['search'] = $search;
        $requeteSearch['type'] = $mangaType;
        $requeteSearch['status'] = $mangaStatus;

        array_unshift($resultSearch,json_decode(Http::get('https://api.jikan.moe/v4/manga?type='.$mangaType.'&q='.$search.'&page='.$numPage.'&status='.$mangaStatus)->getBody()->getContents()));
        array_pop($resultSearch);
        // Return the search view with the resluts compacted
        return view('search', ['requeteSearch'=>$requeteSearch ,'resultSearch' => compact('resultSearch')]);
    }

    public function searchAdvance(Request $request) : View
    {

        $resultSearch[] = array();
        $requeteSearch[] = array();

        // Get the search value from the request
        $search = $request->input('search');
        $numPage = $request->input('numPage');
        $mangaType = $request->input('category');
        $mangaStatus = $request->input('status');
        $scoreMin = $request->input('scoreMin');
        $score = $request->input('score');
        $scoreMax = $request->input('scoreMax');
        $limitPerPage = $request->input('limit');

        $requeteSearch['search'] = $search;
        $requeteSearch['type'] = $mangaType;
        $requeteSearch['status'] = $mangaStatus;

        $requeteValue = $search;

        array_unshift($resultSearch,json_decode(Http::get('https://api.jikan.moe/v4/manga?type='.$mangaType.'&q='.$search.'&page='.$numPage.'&status='.$mangaStatus.'&min_score='.$scoreMin.'&score='.$score.'&max_score='.$scoreMax.'&limit='.$limitPerPage)->getBody()->getContents()));
        array_pop($resultSearch);
        // Return the search view with the resluts compacted
        return view('manga.search', ['requeteSearch'=>$requeteSearch ,
                                            'resultSearch' => compact('resultSearch'),
                                            'requeteValue' => $requeteValue]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() : View
    {
        return view('manga.add');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMangaRequest $request) : RedirectResponse
    {
        //fonction appelé avec une méthode post
        try
        {
            $manga = new Manga;

            $manga->user_id = $request->user_id;
            $manga->manga_id = $request->manga_id;

            $manga->save();

            return Redirect::route('manga.info',['idManga' => $request->manga_id])->with('success','le manga a bien été ajouté a votre liste de favoris');
        }
        catch (Exception $error)
        {
            return Redirect::route('home')->with('error','Erreur rencontré lors de l\'ajout/modification du manga');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id) : View
    {
        $manga = json_decode(Http::get('https://api.jikan.moe/v4/manga/'.$id)->getBody()->getContents());

        $mangaWebsite = json_decode(Http::get('https://api.jikan.moe/v4/manga/'.$id."/external")->getBody()->getContents());

        $Followed = DB::table('mangas')
                            ->where('user_id','=',Auth::id())
                            ->where('manga_id','=',$id)
                            ->get();
        $isFollowed = count($Followed) == 0 ? false : true;
        return view('manga.info',['manga' =>$manga,'followed' => $isFollowed,'providers' => $mangaWebsite]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Manga $manga)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMangaRequest $request, Manga $manga)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request) : RedirectResponse
    {
        //fonction appelé avec une méthode delete
        try
        {
            DB::table('mangas')
                    ->where('user_id','=',Auth::id())
                    ->where('manga_id','=',$request->manga_id)
                    ->delete();

            return Redirect::route('manga.info',['idManga' => $request->manga_id])->with('success','le manga a bien été supprimé de votre liste');
        }
        catch (Exception $error)
        {
            error_log($error);
            return Redirect::route('home')->with('error','Erreur rencontré lors de la suppression du manga');
        }
    }
}
